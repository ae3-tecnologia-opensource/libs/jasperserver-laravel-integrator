<?php

return [
  'server' => [
      'uri' => env('JASPER_SERVER_URL'),
      'username' => env('JASPER_SERVER_USERNAME'),
      'password' => env('JASPER_SERVER_PASSWORD')
  ]
];
